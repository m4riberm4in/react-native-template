import React, { Component } from 'react';
import { View } from 'react-native';
import ItemCart from '../../components/ItemCart.js';
import Item from '../../components/Item.js';
import UserInformation from '../../components/UserInformation.js';
import ShippingInformation from '../../components/ShippingInformation.js';
import ShippingCarrier from '../../components/ShippingCarrier.js';
import PurchaseInformation from '../../components/PurchaseInformation.js';
import NeedToPay from '../../components/NeedToPay.js';


class Home extends Component {
	constructor(props){
		super(props);
	}

	render() {
		const Items = [
				{
					title:'Snack Potato Bee',
					subtitle:'Rumput Laut',
					image:'',
					price:'Rp. 9.000',
					quantity:'Quantity: 10'
				}
		];
		return (
			<View>
				<ItemCart title="Items In Cart" Items={Items} />
				<UserInformation title="User Information" />
				<ShippingInformation title="Shipping Information" />
				<ShippingCarrier title="Shipping Carrier" />
				<PurchaseInformation title="Purchase Information" />
				<NeedToPay title="Need to Pay" />
			</View>
		)
	}
}

export default Home;
