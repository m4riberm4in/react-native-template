import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, ActivityIndicator } from 'react-native';
import { Divider, Button, Image, Icon } from 'react-native-elements'
import Item from './Item.js';
import MobileInputNumber from './MobileInputNumber.js';
import InputCustom from './InputCustom.js';



class ShippingCarrier extends Component {
	constructor(props){
		super(props);

	}

	render() {
		const { title } = this.props;
		return (
			<View style={style.container}>
				 <View>
				 	<Text style={style.title}>{title}</Text>
				</View>
				<TouchableOpacity>
					<View style={style.containerLabel}>
						<View style={{
							flex: 3,
							justifyContent: 'center',
						}}>
							<Text style={[style.contentFont, {fontWeight: 'bold'}]}>JNE YES</Text>
							<Text style={[style.contentFont, {color: 'red'}]}>Rp. 18.000 (1 Workday) </Text>
						</View>
						
						<View style={{
							flex: 1,
							justifyContent: 'flex-end',
							flexDirection: 'row',
						}}>
							<Image  source={{ uri: 'https://image.opencart.com/cache/5970238d1dc82-resize-500x500.jpg' }}
			  						style={{ width: 50, height: 40 }}
			  						PlaceholderContent={<ActivityIndicator /> } />
			  				<View style={{
									flex: 1,
									justifyContent: 'center',
								}}>
			  					<Icon name='chevron-right'/>
			  				</View>
							
						</View>
					</View>
				</TouchableOpacity>
			</View>
		)
	}
}

ShippingCarrier.propTypes = {
	title: PropTypes.string
}

const style = {
	container: {
		marginTop: 10,
		paddingTop: 10,
		paddingBottom: 10,
		backgroundColor:'white',
		paddingLeft: 15,
		paddingRight: 25,
	},
	containerLabel: {
		flex: 1,
		paddingTop: 10,
		paddingBottom: 15,
		flexDirection : 'row',
		justifyContent: 'space-between'
	},
	title: {
		fontWeight: 'bold',
	},
	contentFont: {
		fontSize: 12,
	},
}

export default ShippingCarrier;
