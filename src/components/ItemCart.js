import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';
import { Card, ListItem } from 'react-native-elements'
import Item from './Item.js';



class ItemCart extends Component {
	constructor(props){
		super(props);

	}

	render() {
		const { title, Items } = this.props;
		return (
			<View style={style.container}>
				 <View>
				 	<Text style={style.title}>{title}</Text>
				 	<View>
				 	{
				 		Items.map((item, index) => {
							console.log(item);
				 			return <Item key={index} 
				 						title={item.title}
				 						subtitle={item.subtitle}
				 						price={item.price}
				 						quantity={item.quantity}
				 					/>
				 		})
				 	}
				 	</View>
				</View>
			</View>
		)
	}
}

ItemCart.propTypes = {
	title: PropTypes.string,
	Items: PropTypes.array,
}

const style = {
	container: {
		marginTop: 10,
		paddingTop: 10,
		paddingBottom: 10,
		backgroundColor:'white',
		paddingLeft: 15,
		paddingRight: 25,
	},
	title: {
		fontWeight: 'bold',
	}
}

export default ItemCart;
