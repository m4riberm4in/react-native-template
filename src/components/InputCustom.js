import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, TextInput } from 'react-native';
import { Card, ListItem, Divider, Input } from 'react-native-elements'
import Item from './Item.js';



class InputCustom extends Component {
	constructor(props){
		super(props);

	}

	render() {
		const { label, placeholder } = this.props;
		return (
			<React.Fragment>
				<Text style={{fontSize: 12, paddingTop: 15}}>{label}</Text>
				<View style={style.containerInput}>
					<TextInput
						style={{
							fontSize: 12
						}}
					  placeholder={placeholder}
					/>
				</View>
			</React.Fragment>
		)
	}
}

InputCustom.propTypes = {
	label: PropTypes.string,
	placeholder: PropTypes.string,
}

const style = {
	containerInput: {
		flex: 1,
		marginTop: 5,
		flexDirection : 'row',
		justifyContent: 'flex-start',
		borderColor: 'gray', 
		borderWidth: 0.5,
		borderRadius: 5,
		height: 35,
		fontSize:12,
	}
}

export default InputCustom;
