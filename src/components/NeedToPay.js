import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, ActivityIndicator } from 'react-native';
import { Divider, Button, Image, Icon } from 'react-native-elements'
import Item from './Item.js';
import MobileInputNumber from './MobileInputNumber.js';
import InputCustom from './InputCustom.js';



class NeedToPay extends Component {
	constructor(props){
		super(props);

	}

	render() {
		const { title } = this.props;
		return (
			<View style={style.container}>
				 <View style={[style.containerLabel, {paddingBottom: 10,}]}>
				 	<Text style={style.title}>{title}</Text>
				 	<Text style={style.title}>Rp. 38.000</Text>
				</View>
				<View style={{paddingTop: 10}}>
					<Button title='Checkout'
							buttonStyle={{backgroundColor:'red'}}
							titleStyle={{fontSize: 12}} />
				</View>
			</View>
		)
	}
}

NeedToPay.propTypes = {
	title: PropTypes.string
}

const style = {
	container: {
		marginTop: 10,
		paddingTop: 10,
		paddingBottom: 10,
		backgroundColor:'white',
		paddingLeft: 15,
		paddingRight: 25,
	},
	containerLabel: {
		flex: 1,
		paddingTop: 10,
		paddingBottom: 15,
		flexDirection : 'row',
		justifyContent: 'space-between'
	},
	title: {
		fontWeight: 'bold',
	},
	contentFont: {
		fontSize: 12,
	},
}

export default NeedToPay;
