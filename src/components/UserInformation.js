import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, TextInput } from 'react-native';
import { Divider, Button } from 'react-native-elements'
import Item from './Item.js';
import MobileInputNumber from './MobileInputNumber.js';
import InputCustom from './InputCustom.js';



class UserInformation extends Component {
	constructor(props){
		super(props);

	}

	render() {
		const { title, Items } = this.props;
		return (
			<View style={style.container}>
				 <View>
				 	<Text style={style.title}>{title}</Text>
				</View>
				<View style={style.containerLabel}>
					<Text style={style.contentFont}>Already a user? </Text>
					<TouchableOpacity>
						<Text style={style.labelLogin}>Login to your account</Text>
					</TouchableOpacity>
				</View>
				<Divider />
				<MobileInputNumber codeArea='+62' />
				<InputCustom label='Full Name' placeholder='Enter Your Full Name'/>
				<View style={{paddingTop: 10}}>
					<Button title='Save User Information'
							buttonStyle={{backgroundColor:'red'}}
							titleStyle={{fontSize: 12}} />
				</View>
			</View>
		)
	}
}

UserInformation.propTypes = {
	title: PropTypes.string,
}

const style = {
	container: {
		marginTop: 10,
		paddingTop: 10,
		paddingBottom: 10,
		backgroundColor:'white',
		paddingLeft: 15,
		paddingRight: 25,
	},
	containerLabel: {
		flex: 1,
		paddingTop: 10,
		paddingBottom: 15,
		flexDirection : 'row',
	},
	containerInput: {
		flex: 1,
		marginTop: 10,
		flexDirection : 'row',
		justifyContent: 'flex-start',
		borderColor: 'gray', 
		borderWidth: 0.5,
		borderRadius: 5,
		height: 40,
		fontSize:12,
	},
	title: {
		fontWeight: 'bold',
	},
	contentFont: {
		fontSize: 12,
	},
	labelLogin: {
		fontSize: 12,
		color: 'red'
	}
}

export default UserInformation;
