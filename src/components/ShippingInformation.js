import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, ActivityIndicator, Picker, StyleSheet} from 'react-native';
import { Divider, Button, Image, Icon } from 'react-native-elements'
import Item from './Item.js';
import MobileInputNumber from './MobileInputNumber.js';
import InputCustom from './InputCustom.js';
import RNPickerSelect from 'react-native-picker-select';



class ShippingInformation extends Component {
	constructor(props){
		super(props);
		this.state = {
			province: '',
			city: '',
			district: '',
		}
	}

	render() {
		const { title } = this.props;
		const sports = [
		  {
		    label: 'Football',
		    value: 'football',
		  },
		  {
		    label: 'Baseball',
		    value: 'baseball',
		  },
		  {
		    label: 'Hockey',
		    value: 'hockey',
		  },
		];
		return (
			<View style={style.container}>
				<View>
				 	<Text style={style.title}>{title}</Text>
				</View>
				<View style={style.containerForm}>
					<View>
						<Text style={style.contentFont}>Province</Text>
						<View style={{
							flexDirection : 'column',
							justifyContent: 'center',
							borderColor: 'gray',
							borderWidth: 0.5,
							borderRadius: 5,
							height:35,
							marginRight:5,
						}}>
							<Picker
							  selectedValue={this.state.province}
							  style={{height: 20, width: 89}}
							  onValueChange={(itemValue, itemIndex) =>
							    this.setState({province: itemValue})
							  }>
							  <Picker.Item label="DKI Jakarta" value="1" />
							  <Picker.Item label="Jawa Barat" value="2" />
							</Picker>
						</View>
						
					</View>
					<View>
						<Text style={style.contentFont}>City</Text>
						<View style={{
							flexDirection : 'column',
							justifyContent: 'center',
							borderColor: 'gray',
							borderWidth: 0.5,
							borderRadius: 5,
							height:35,
							marginRight:5,
						}}>
							<Picker
							  selectedValue={this.state.province}
							  style={{height: 20, width: 89}}
							  onValueChange={(itemValue, itemIndex) =>
							    this.setState({province: itemValue})
							  }>
							  <Picker.Item label="DKI Jakarta" value="1" />
							  <Picker.Item label="Jawa Barat" value="2" />
							</Picker>
						</View>
					</View>
					<View>
						<Text style={style.contentFont}>District</Text>
						<View style={{
							flexDirection : 'column',
							justifyContent: 'center',
							borderColor: 'gray',
							borderWidth: 0.5,
							borderRadius: 5,
							height:35,
							marginRight:5,
						}}>
							<Picker
							  selectedValue={this.state.province}
							  style={{height: 20, width: 89}}
							  onValueChange={(itemValue, itemIndex) =>
							    this.setState({province: itemValue})
							  }>
							  <Picker.Item label="DKI Jakarta" value="1" />
							  <Picker.Item label="Jawa Barat" value="2" />
							</Picker>
						</View>
					</View>
				</View>
				<InputCustom label='Street Address' placeholder='Enter Street Address' />
				<View style={{paddingTop: 10}}>
					<Button title='Save Address'
							buttonStyle={{backgroundColor:'red'}}
							titleStyle={{fontSize: 12}} />
				</View>
			</View>
		)
	}
}

ShippingInformation.propTypes = {
	title: PropTypes.string
}

const style = {
	container: {
		marginTop: 10,
		paddingTop: 10,
		paddingBottom: 10,
		backgroundColor:'white',
		paddingLeft: 15,
		paddingRight: 25,
	},
	containerForm: {
		flex:1,
		flexDirection: 'row',
		paddingTop: 10,
		paddingRight: 25,
	},
	containerLabel: {
		flex: 1,
		paddingTop: 10,
		paddingBottom: 15,
		flexDirection : 'row',
		justifyContent: 'space-between'
	},
	title: {
		fontWeight: 'bold',
	},
	contentFont: {
		fontSize: 12,
		paddingBottom:5,
	},
}

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});

export default ShippingInformation;
