import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, ActivityIndicator, CheckBox } from 'react-native';
import { Divider, Button, Image, Icon } from 'react-native-elements'
import Item from './Item.js';
import MobileInputNumber from './MobileInputNumber.js';
import InputCustom from './InputCustom.js';



class PurchaseInformation extends Component {
	constructor(props){
		super(props);
		this.state = {
			muchoBalance: false,
		}

	}

	render() {
		const { title } = this.props;
		return (
			<View style={style.container}>
				 <View style={{
				 	paddingBottom: 10,
				 }}>
				 	<Text style={style.title}>{title}</Text>
				</View>
				 <View style={style.containerLabel}>
				 	<Text style={style.contentFont}>Total Product Price</Text>
				 	<Text style={style.contentFont}>Rp. 5000</Text>
				</View>
				 <View style={[style.containerLabel, {paddingBottom: 10,}]}>
				 	<Text style={style.contentFont}>Total Product Discount</Text>
				 	<Text style={[style.contentFont, {color:'red'}]}>(Rp. 15.000)</Text>
				</View>
				<Divider />
				 <View style={[style.containerLabel, {paddingBottom: 10,}]}>
				 	<Text style={style.title}>SUB TOTAL</Text>
				 	<Text style={style.contentFont}>Rp. 38.000</Text>
				</View>
				 <View style={[style.containerLabel, {paddingBottom: 10,}]}>
				 	<View style={{
				 		flexDirection: 'row',
				 	}}>
					    <CheckBox
					      value={this.state.muchoBalance}
					      style={{
					      	marginLeft: 0,
					      	color:'red'
					      }}
					      onValueChange={() => this.setState({ muchoBalance: !this.state.muchoBalance })}
					    />
						<Text style={[style.contentFont, {marginTop:5}]}>Mucho Balance </Text>    
						<Text style={[style.contentFont, {color:'green', marginTop:5}]}>[Rp. 12.000]</Text>    
				 	</View>
				 	<Text style={[style.contentFont, {marginTop:5}]}>Not Applied</Text>
				</View>
			</View>
		)
	}
}

PurchaseInformation.propTypes = {
	title: PropTypes.string
}

const style = {
	container: {
		marginTop: 10,
		paddingTop: 10,
		paddingBottom: 10,
		backgroundColor:'white',
		paddingLeft: 15,
		paddingRight: 25,
	},
	containerLabel: {
		flex: 1,
		flexDirection : 'row',
		justifyContent: 'space-between'
	},
	title: {
		fontWeight: 'bold',
	},
	contentFont: {
		fontSize: 12,
	},
}

export default PurchaseInformation;
