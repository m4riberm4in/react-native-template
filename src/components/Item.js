import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, ActivityIndicator, TouchableOpacity } from 'react-native';
import { Image } from 'react-native-elements'


class Item extends Component {
	constructor(props){
		super(props);

	}

	render() {
		const { title, price, subtitle, quantity } = this.props;
		return (
			<View style={style.container}>
  				<View style={style.itemContainer}>
  					<View style={style.imageContainer}>
						<Image  source={{ uri: 'https://www.minimeinsights.com/wp-content/uploads/2016/11/Potabee.jpg' }}
		  						style={{ width: 45, height: 45 }}
		  						PlaceholderContent={<ActivityIndicator /> } />
  					</View>
	  				<View style={style.labelContainer}>

	  					<Text style={style.titleItem}>{title}</Text>
	  					<Text style={style.subtitleItem}>{subtitle}</Text>
	  					<View style={style.bottomLabelContainer}>
	  						<Text style={[{color:'red'}, style.titleItem]}>{price}</Text>	
	  						<Text style={style.titleItem}>{quantity}</Text>	
	  					</View>
	  				</View>
  				</View>
				<TouchableOpacity>
					<Text style={style.textNoted}>Tambah Catatan Untuk Penjual</Text>	
				</TouchableOpacity>
			</View>
			)
	
	}
}

Item.propTypes = {
	image: PropTypes.string,
	title: PropTypes.string,
	subtitle: PropTypes.string,
	price: PropTypes.string,
	quantity: PropTypes.string,
}

const style = {
	container: {
		flex: 1,
		paddingTop: 10,
	},
	imageContainer: {
		paddingTop: 5,
	},
	itemContainer : {
		flex: 1,
		flexDirection: 'row',
		paddingTop: 2,
	},
	labelContainer:{
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'flex-start',
		paddingLeft: 5,
	},
	titleItem: {
		fontSize: 12,
	},
	subtitleItem: {
		fontSize: 12,
		color:'gray',
	},
	bottomLabelContainer: {
		flex:1,
		flexDirection: 'row',
		justifyContent: 'space-between'
	},
	textNoted: {
		paddingTop: 5,
		fontSize: 10,
		color: 'gray',
		textDecorationLine: 'underline',
	}
}

export default Item;
