import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, TextInput } from 'react-native';
import { Card, ListItem, Divider, Input } from 'react-native-elements'
import Item from './Item.js';



class MobileInputNumber extends Component {
	constructor(props){
		super(props);

	}

	render() {
		const { codeArea } = this.props;
		return (
			<React.Fragment>
				<Text style={{fontSize: 12, paddingTop: 15}}>Mobile Number</Text>
				<View style={style.containerInput}>
					<View style={{
						justifyContent:'center',
						backgroundColor:'lightgray',
						borderRadiusLeft:15,
						width: 35,
						height: 34,
					}}>
						<Text style={{textAlign: 'center', fontSize:12}}>{codeArea}</Text>
					</View>
					<TextInput
						style={{
							fontSize: 12
						}}
					  placeholder='Enter Mobile Number'
					/>
				</View>
			</React.Fragment>
		)
	}
}

MobileInputNumber.propTypes = {
	codeArea: PropTypes.string,
}

const style = {
	containerInput: {
		flex: 1,
		marginTop: 5,
		flexDirection : 'row',
		justifyContent: 'flex-start',
		borderColor: 'gray', 
		borderWidth: 0.5,
		borderRadius: 5,
		height: 35,
		fontSize:12,
	}
}

export default MobileInputNumber;
