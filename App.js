/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import { Header } from 'react-native-elements'

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';

import Home from './src/containers/Home/Home.js';

const App: () => React$Node = () => {
  return (
      <React.Fragment>
        <Header
            leftComponent={{ icon: 'chevron-left'}}
            centerComponent={{ text: 'Order Confirmation' }}
            containerStyle={{
            backgroundColor: '#ffffff',
            justifyContent: 'space-around',
          }}
        />
        <ScrollView>
          <SafeAreaView>
            <Home />
            </SafeAreaView>
        </ScrollView> 
    </React.Fragment>  
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
    margin: 0,
    flex: 1,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
